

const findByIp = document.getElementById("find-by-ip");
const app = document.getElementById("app");
findByIp.addEventListener("click", findUserByIp);

async function findUserByIp() {
  app.innerHTML = "";
  try {
    let ipResponse = await fetch("https://api.ipify.org/?format=json");
    let ipData = await ipResponse.json();
    let ipAddress = ipData.ip;

    let locationResponse = await fetch(
      `https://ip-api.com/json/${ipAddress}?fields=continent,country,regionName,city,district`
    );
    let locationData = await locationResponse.json();

    const continent = document.createElement("p");
    continent.innerText = "Континент: " + locationData.continent;
    const country = document.createElement("p");
    country.innerText = "Страна: " + locationData.country;
    const region = document.createElement("p");
    region.innerText = "Область: " + locationData.regionName;
    const city = document.createElement("p");
    city.innerText = "Город: " + locationData.city;
    const district = document.createElement("p");
    district.innerText =
      "Район: " + (locationData.district || "Район неизвестен");

    app.append(continent, country, region, city, district);
  } catch (error) {
    console.error(error);
    const errorMessage = document.createElement("p");
    errorMessage.innerText =
      "Произошла ошибка при получении информации.";
    app.append(errorMessage);
  }
}
