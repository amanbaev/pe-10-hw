const modalSettings = [
    {
      modalId: "addToBasket",
      settings: {
        header: "Add to Basket",
        closeButton: true,
        text: "Continue",
        actions: [
          {
            type: "submit",
            backgroundColor: "pink",
            text: "submit",
          },
          {
            type: "cancel",
            backgroundColor: "red",
            text: "cancel",
          },
        ],
      },
    },
  ];
  
  export default modalSettings;
  