const modalSettings = [
  {
    modalId: "default",
    settings: {
      header: "",
      closeButton: true,
      text: (data) => data,
      actions: [
        {
          type: "submit",
          backgroundColor: "white",
          text: "",
        },
        {
          type: "cancel",
          backgroundColor: "white",
          text: "",
        },
      ],
    },
  },
  {
    modalId: "addToBasket",
    settings: {
      header: "Adding a product to the cart",
      closeButton: true,
      text: (data) => `You are trying to add ${data} to the basket, Continue?`,
      actions: [
        {
          type: "submit",
          backgroundColor: "pink",
          text: "LetsGo!",
        },
        {
          type: "cancel",
          backgroundColor: "red",
          text: "No, I changed my mind",
        },
      ],
    },
  },
  {
    modalId: "removeFromBasket",
    settings: {
      header: "Removing the product from the cart",
      closeButton: true,
      text: (data) => `You are trying to delete ${data} from the recycle bin. Do we do?`,
      actions: [
        {
          type: "submit",
          backgroundColor: "pink",
          text: "Yes!",
        },
        {
          type: "cancel",
          backgroundColor: "red",
          text: "Let it be",
        },
      ],
    },
  },
];

export default modalSettings;
