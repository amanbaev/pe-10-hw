import { useState, useEffect } from "react";
import PropTypes from "prop-types";
import Button from "../UI/button/Button";
import styles from "./Product.module.scss";
import favoriteStar from "../../image/favourites-star.png";
import favoriteStarBlack from "../../image/favourites-star-black.png";

function Product({ openModal, updateFavorite, updateBasket, ...props }) {
  const [inBasket, setInBasket] = useState(false);
  const [inFavorite, setInFavorite] = useState(false);
  const [product, setProduct] = useState({});
  const [page, setPage] = useState("");

  useEffect(() => {
    setProduct(props.product);
    const productsInFavoriteArray = JSON.parse(
      localStorage.getItem("productsInFavorite")
    );
    const productInFavorite = productsInFavoriteArray.find(
      (item) => item.article === props.product.article
    );
    productInFavorite && setInFavorite(true);

    const productsInBasketArray = JSON.parse(
      localStorage.getItem("productsInBasket")
    );
    const productInBasket = productsInBasketArray.find(
      (product) => product.article === props.product.article
    );
    productInBasket && setInBasket(true);
  }, [props.product]);

  useEffect(() => {
    setPage(props.page);
  }, [props.page]);

  const addToFavorite = () => {
    const productsInFavoriteArray = JSON.parse(
      localStorage.getItem("productsInFavorite")
    );
    const productInFavorite = productsInFavoriteArray.find(
      (item) => item.article === product.article
    );
    if (productInFavorite) return false;
    localStorage.setItem(
      "productsInFavorite",
      JSON.stringify([...productsInFavoriteArray, product])
    );
    setInFavorite(true);
    updateFavorite();
  };

  const removeFromFavorite = () => {
    const productsInFavoriteArray = JSON.parse(
      localStorage.getItem("productsInFavorite")
    );
    const removedProductIndex = productsInFavoriteArray.findIndex(
      (item) => item.article === product.article
    );
    const updatedProductsInFavoriteArray = [
      ...productsInFavoriteArray.slice(0, removedProductIndex),
      ...productsInFavoriteArray.slice(removedProductIndex + 1),
    ];
    localStorage.setItem(
      "productsInFavorite",
      JSON.stringify(updatedProductsInFavoriteArray)
    );
    setInFavorite(false);
    updateFavorite();
  };

  const toggleFavorite = () => {
    inFavorite ? removeFromFavorite() : addToFavorite();
  };

  const addToBasket = () => {
    const productsInBasketArray = JSON.parse(
      localStorage.getItem("productsInBasket")
    );
    const productInBasket = productsInBasketArray.find(
      (item) => item.article === product.article
    );
    if (productInBasket) return false;
    localStorage.setItem(
      "productsInBasket",
      JSON.stringify([...productsInBasketArray, product])
    );
    setInBasket(true);
    updateBasket();
  };

  const removeFromBasket = () => {
    const productsInBasketArray = JSON.parse(
      localStorage.getItem("productsInBasket")
    );
    const removedProductIndex = productsInBasketArray.findIndex(
      (item) => item.article === product.article
    );
    const updatedProductsInBasketArray = [
      ...productsInBasketArray.slice(0, removedProductIndex),
      ...productsInBasketArray.slice(removedProductIndex + 1),
    ];
    localStorage.setItem(
      "productsInBasket",
      JSON.stringify(updatedProductsInBasketArray)
    );
    setInBasket(false);
    updateBasket();
  };

  const { title, price, imgUrl, color } = product;
  return (
    <div className={styles.Product}>
      <div className={styles.Favorite} onClick={toggleFavorite}>
        <img src={inFavorite ? favoriteStarBlack : favoriteStar} alt="" />
      </div>
      <div className={styles.imageWrapper}>
        <img src={imgUrl} alt="" />
      </div>
      <div>
        <h2>{title}</h2>
        <h4>Колір: {color}</h4>
        <h3>Ціна: {price} грн</h3>
      </div>
      <Button
        text={
          (page === "basket" && "Видалити?") ||
          (inBasket ? "Already in the cart" : "To the basket")
        }
        backgroundColor={inBasket ? "grey" : "yellow"}
        onClick={() => {
          (page === "home" || page === "favorite") && !inBasket
            ? openModal("addToBasket", addToBasket, product.title)
            : page === "basket" &&
              openModal("removeFromBasket", removeFromBasket, product.title);
        }}
      />
    </div>
  );
}

Product.propTypes = {
  product: PropTypes.object,
  updateFavorite: PropTypes.func,
  updateBasket: PropTypes.func,
  openModal: PropTypes.func,
};

export default Product;
