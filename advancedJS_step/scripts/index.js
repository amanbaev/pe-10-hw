import {getToken, sendCard, deleteCard, getCards, getCard, editCard} from './functions/send-request.js';
import {Modal, ModalLogin, ModalAddCard, ModalEditCard} from './classes/modal.js';
import {renderCards, Visit, VisitCardiologist, noItems, renderNewCard,cardsWrapper } from './classes/cards.js';
import formToObj from './functions/form-to-obj.js';
import dragAndDrop from './functions/drag-and-drop.js';
import searchFilter from './functions/search-filter.js';

const API = 'https://ajax.test-danit.com/api/v2/cards';
let visitsCollection = [];

let entryModal; 
let keyToken; 
let newVisitModal;
let editVisitModal;

window.addEventListener("load", () => { 
    keyToken = localStorage.getItem('token'); 
    if (keyToken) {
        document.querySelector('#entry-btn').classList.add('hidden');
        document.querySelector('#visit-btn').classList.remove('hidden');
        document.querySelector('#logout-btn').classList.remove('hidden');
        document.querySelector('#sorting-form').classList.remove('hidden');

        visitsCollection = JSON.parse(localStorage.getItem('allVisits')) 
        searchFilter(visitsCollection)
        renderCards(visitsCollection);
        noItems(visitsCollection);
    } 
});
    

document.addEventListener('click', async (e) => {
    // e.preventDefault();
    if (e.target.id === 'entry-btn') {
                
        entryModal = new ModalLogin();
        entryModal.render();
    } else if (e.target.id === 'login-btn') {    
        e.preventDefault();
        let login = document.querySelector('#inputEmail').value;
        let password = document.querySelector('#inputPassword').value;
    
        if(login.includes('@') && password) {
        
            await getToken(API, login, password)
            .then(token => {
                
                if (token && typeof token !== 'object') {
                    localStorage.setItem('token', token) 
                    keyToken = localStorage.getItem('token') 
                    entryModal.close()                    
                } else {                                    
                   
                    entryModal.invalid();
                }
            })
            .catch(e => console.log(e.message))     
                
        } else {                      
            entryModal.invalid();
        }
     
        if(keyToken) { 
            document.querySelector('#entry-btn').classList.add('hidden');
            document.querySelector('#visit-btn').classList.remove('hidden');
            document.querySelector('#logout-btn').classList.remove('hidden');
            // показуємо форму пошуку
            document.querySelector('#sorting-form').classList.remove('hidden');

            await getCards(API, keyToken).then(cardsList => {
                localStorage.setItem('allVisits', JSON.stringify(cardsList))
                visitsCollection = JSON.parse(localStorage.getItem('allVisits'))
            });
            searchFilter(visitsCollection)
            renderCards(visitsCollection);
            noItems(visitsCollection);
        }

        
    } else if (e.target.id === 'visit-btn') {             
        newVisitModal = new ModalAddCard();
        newVisitModal.render();
    } else if (e.target.id === 'create-btn') {                
        e.preventDefault();
        const form = document.querySelector('#newVisitForm');
        form.classList.add('was-validated'); 
        if(form.checkValidity()) {

            let formData = new FormData(form)

            let visitData = formToObj(formData); 
            visitData.status = 'Open'

            newVisitModal.close();
            

            await sendCard(API, keyToken, visitData).then(card => {
                visitsCollection.push(card);
                localStorage['allVisits'] = JSON.stringify(visitsCollection); 
                renderNewCard(card);
            });
        }
        
    } else if (e.target.id === 'deleteBtn') {                    
        const card = e.target.closest('.visit-card')
        const cardId = card.getAttribute('data-id')
        await deleteCard(API, keyToken, cardId)
            .then(response => {
                if(response) {
                    visitsCollection.forEach(data => {
                        const cardIndex = visitsCollection.indexOf(data)
                        if (data.id == cardId) {
                            visitsCollection.splice(cardIndex, 1)
                            localStorage['allVisits'] = JSON.stringify(visitsCollection);  // перезаписуємо в localStorage наші зміни
                            console.log(visitsCollection);
                        }
                    })
                    card.remove()
                    noItems(visitsCollection);
                }
            })
    } else if (e.target.id === 'editBtn') {                      
        let visit = visitsCollection.filter(visit => visit.id === +e.target.closest('.visit-card').dataset.id)[0];
        
        editVisitModal = new ModalEditCard(visit);
        editVisitModal.render();
    } else if (e.target.id === 'saveChanges-btn') {         
        e.preventDefault();
        const form = document.querySelector('#editVisitForm');
        form.classList.add('was-validated');  
        if(form.checkValidity()) {
            let formData = new FormData(form)
            let visitData = formToObj(formData); 
            visitData.status = 'Open'
            editVisitModal.close();
    
            await editCard(API, keyToken, editVisitModal.id, visitData).then(card => {

                let editedVidit = visitsCollection.find(visit => visit.id === card.id);
                let index = visitsCollection.indexOf(editedVidit);
                visitsCollection[index] = card;

                localStorage['allVisits'] = JSON.stringify(visitsCollection);  // перезаписуємо в localStorage наші зміни
                
                
      
                document.querySelectorAll('.visit-card').forEach(el => {
                    if(+el.dataset.id === card.id) {
                        el.remove();
                    }
                })
                
        
                renderNewCard(card);
                
            });
        }
    } else if (e.target.id === 'logout-btn') { 
        localStorage.clear();  
        location.reload()      
    }  else if (e.target.id === 'showMore') {
        e.target.closest('.visit-card').classList.toggle('card-border-radius')
        e.target.closest('.visit-card').classList.toggle('card-z-index')
    } else if (e.target.id === 'statusDone') {
        const card = e.target.closest('.visit-card')
        const cardAction = card.querySelector('#card-action')
        const cardId = +card.getAttribute('data-id')
        const cardStatus = card.querySelector('.card-status')
        const visitData = visitsCollection.find(visit => visit.id === cardId)
        visitData.status = 'Done'
        
        await editCard(API, keyToken, cardId, visitData).then(card => {
            cardStatus.innerHTML = 'Status: Done'
            e.target.classList.add('btnDone')
            cardAction.classList.add('justify-content-end')
            let index = visitsCollection.indexOf(card);
                visitsCollection[index] = card;
                localStorage['allVisits'] = JSON.stringify(visitsCollection);
        })
        }
    
})


dragAndDrop()
