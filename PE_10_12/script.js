let btnEnter = document.getElementById("btnEnter");
let btnS = document.getElementById("btnS");
let btnE = document.getElementById("btnE");
let btnO = document.getElementById("btnO");
let btnN = document.getElementById("btnN");
let btnL = document.getElementById("btnL");
let btnZ = document.getElementById("btnZ");

document.addEventListener("keydown", (event) => {
  if (event.code == "Enter") {
    btnEnter.style.backgroundColor = 'blue'
  } else if (event.code == "KeyS") {
    btnS.style.backgroundColor = 'blue'
  } else if (event.code == "KeyE") {
    btnE.style.backgroundColor = 'blue'
  } else if (event.code == "KeyO") {
    btnO.style.backgroundColor = 'blue'
  } else if (event.code == "KeyN") {
    btnN.style.backgroundColor = 'blue'
  } else if (event.code == "KeyL") {
    btnL.style.backgroundColor = 'blue'
  } else if (event.code == "KeyZ") {
    btnZ.style.backgroundColor = 'blue'
  }
});
document.addEventListener("keyup", (event) => {
    if (event.code == "Enter") {
      btnEnter.style.backgroundColor = 'black'
    } else if (event.code == "KeyS") {
      btnS.style.backgroundColor = 'black'
    } else if (event.code == "KeyE") {
      btnE.style.backgroundColor = 'black'
    } else if (event.code == "KeyO") {
      btnO.style.backgroundColor = 'black'
    } else if (event.code == "KeyN") {
      btnN.style.backgroundColor = 'black'
    } else if (event.code == "KeyL") {
      btnL.style.backgroundColor = 'black'
    } else if (event.code == "KeyZ") {
      btnZ.style.backgroundColor = 'black'
    }
  });
  

