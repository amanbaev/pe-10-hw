let firstArr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
let secondArr = ["1", "2", "3", "sea", "user", 23];
let vstavka = document.getElementById('divf')

function func(array,parent = document.body) {
    let list = document.createElement("ul");
    let liObvertka = array.map((item) => {
        return `<li>${item}</li>`;
    });
    liObvertka = liObvertka.join('');
    list.innerHTML = liObvertka;
      parent.prepend(list)
} 
func(firstArr);
func(secondArr,vstavka);


// 1)Теги можно писать как прописными, так и строчными символами
// Любые теги, а также их атрибуты нечувствительны к регистру, 
// поэтому вы вольны выбирать сами, как писать — <BR>, <Br> или <br>.

// 2)insertAdjacentHTML() разбирает указанный текст как HTML или XML и 
// вставляет полученные узлы (nodes) в DOM дерево в указанную позицию.

// 3) let description = document. getElementById("description"); 
// description.