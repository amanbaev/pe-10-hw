
const root = document.getElementById("root");

function createElement(tagName, className) {
  const element = document.createElement(tagName);
  element.className = className;
  return element;
}

async function fetchFilms() {
  try {
    const response = await fetch(
      "https://ajax.test-danit.com/api/swapi/films",
      {
        method: "GET",
      }
    );
    const filmsArray = await response.json();
    const sortedFilmsArray = [...filmsArray].sort(
      (a, b) => a.episodeId - b.episodeId
    );

    sortedFilmsArray.forEach(async (film) => {
      const list = createElement("ul", "film");
      root.append(list);
      const episodeName = createElement("li", "episode-name");
      episodeName.innerText = `Episode ${film.episodeId}. ${film.title}`;
      const episodeDescription = createElement("li", "episode-description");
      episodeDescription.innerText = film.openingCrawl;
      const characters = createElement("li", "characters");
      const charactersList = createElement("ul", "characters-list");
      charactersList.insertAdjacentHTML(
        "beforeend",
        `<li>Characters:</li>`
      );
      characters.append(charactersList);
      const loadingAnimation = createElement(
        "div",
        `animation animation-${film.episodeId}`
      );
      list.append(
        episodeName,
        loadingAnimation,
        characters,
        episodeDescription
      );

      for (const characterUrl of film.characters) {
        try {
          const response = await fetch(characterUrl, {
            method: "GET",
          });
          const character = await response.json();
          charactersList.insertAdjacentHTML(
            "beforeend",
            `<li>${character.name}</li>`
          );
          loadingAnimation.remove();
        } catch (error) {
          console.error(error);
        }
      }
    });
  } catch (error) {
    console.error(error);
  }
}

fetchFilms();
