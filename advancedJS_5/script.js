
fetch('https://ajax.test-danit.com/api/json/users')
  .then(response => response.json())
  .then(users => {
    const usersData = {};
    users.forEach(user => {
      usersData[user.id] = user;
    });

    fetch('https://ajax.test-danit.com/api/json/posts')
      .then(response => response.json())
      .then(posts => {
        const postsContainer = document.querySelector('#posts-container');

        posts.forEach(post => {
          const postElement = document.createElement('div');
          postElement.classList.add('card');

          const titleElement = document.createElement('h3');
          titleElement.textContent = post.title;
          const bodyElement = document.createElement('p');
          bodyElement.textContent = post.body;

          const user = usersData[post.userId];
          const authorElement = document.createElement('div');
          authorElement.classList.add('author');
          authorElement.innerHTML = `
            <span class="name">${user.name}</span>
            <span class="email">${user.email}</span>
          `;


          const deleteButton = document.createElement('button');
          deleteButton.classList.add('delete-button');
          deleteButton.innerText = 'delete';
          deleteButton.addEventListener('click', () => {
            fetch(`https://ajax.test-danit.com/api/json/posts/${post.id}`, { method: 'DELETE' })
              .then(response => {
                if (response.ok) {
                  postElement.remove();
                } else {
                  console.error('ERROR', response.status);
                }
              })
              .catch(error => {
                console.error('ERROR', error);
              });
          });
          
          postElement.appendChild(deleteButton);
          postElement.appendChild(titleElement);
          postElement.appendChild(bodyElement);
          postElement.appendChild(authorElement);
          postsContainer.appendChild(postElement);



        });
      })
      .catch(error => {
        console.error('Ошибка при получении списка публикаций:', error);
      });
  })
  .catch(error => {
    console.error('Ошибка при получении списка пользователей:', error);
  })
 


 


