// 1. мы можем использовать try...catch,
// чтобы обработать возможные некорректные данные и 
// выполнить необходимые действия.
// 2. мы  можем использовать try...catch, чтобы обработать возможные ошибки, 
// такие как отсутствие файла или ошибка доступа.
// 3. мы можем использовать try...catch, чтобы обработать эту ошибку 
// и выполнить необходимые действия.
const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчет",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];
  
  const root = document.getElementById("root");
  const ul = document.createElement("ul");
  root.appendChild(ul);
  
  for (let i = 0; i < books.length; i++) {
    const book = books[i];
  
    try {
      if (!book.author || !book.name || !book.price) {
        throw new Error(`The book with index ${i} doesn't have one of the required properties`);
      }
  
      const li = document.createElement("li");
      li.innerHTML = `Author: ${book.author}, Name: ${book.name}, Price: ${book.price}`;
      ul.appendChild(li);
    } catch (error) {
      console.error(error);
    }
  }