import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { openModal } from "../../redux/actions/magazine";
import {
  addProductToBasket,
  addProductToFavorite,
  removeProductFromFavorite,
  removeProductFromBasket,
} from "../../redux/actions/magazine";
import PropTypes from "prop-types";
import Button from "../UI/button/Button";
import styles from "./Product.module.scss";
import favoriteStar from "../../image/favourites-star.png";
import favoriteStarBlack from "../../image/favourites-star-black.png";

function Product({ updateFavorite, updateBasket, ...props }) {
  const [inBasket, setInBasket] = useState(false);
  const [inFavorite, setInFavorite] = useState(false);
  const [product, setProduct] = useState({});
  const [page, setPage] = useState("");

  const products = useSelector((state) => state.magazine.products);
  const dispatch = useDispatch();

  useEffect(() => {
    setProduct(props.product);
    const productInFavorite = [...products.productsInFavorite].find(
      (item) => item.article === props.product.article
    );
    productInFavorite && setInFavorite(true);

    const productInBasket = [...products.productsInBasket].find(
      (product) => product.article === props.product.article
    );
    productInBasket && setInBasket(true);
  }, [props.product, products.productsInBasket, products.productsInFavorite]);

  useEffect(() => {
    setPage(props.page);
  }, [props.page]);

  const addToFavorite = () => {
    dispatch(addProductToFavorite(product));
    setInFavorite(true);
  };

  const removeFromFavorite = () => {
    dispatch(removeProductFromFavorite(product.article));
    setInFavorite(false);
  };

  const addToBasket = () => {
    dispatch(addProductToBasket(product));
    setInBasket(true);
  };

  const removeFromBasket = () => {
    dispatch(removeProductFromBasket(product.article));
    setInBasket(false);
  };

  const { title, price, imgUrl, color } = product;
  
  return (
    <div className={styles.Product}>
      <div
        className={styles.Favorite}
        onClick={() => {
          inFavorite ? removeFromFavorite() : addToFavorite();
        }}
      >
        <img src={inFavorite ? favoriteStarBlack : favoriteStar} alt="" />
      </div>
      <div className={styles.imageWrapper}>
        <img src={imgUrl} alt="" />
      </div>
      <div>
        <h2>{title}</h2>
        <h4>Колір: {color}</h4>
        <h3>Ціна: {price} грн</h3>
      </div>
      <Button
        text={
          (page === "basket" && "Видалити?") ||
          (inBasket ? "Вже в кошику" : "До кошика")
        }
        backgroundColor={inBasket ? "grey" : "yellow"}
        onClick={() => {
          (page === "home" || page === "favorite") && !inBasket
            ? dispatch(openModal("addToBasket", addToBasket, product.title))
            : page === "basket" &&
              dispatch(
                openModal("removeFromBasket", removeFromBasket, product.title)
              );
        }}
      />
    </div>
  );
}

Product.propTypes = {
  product: PropTypes.object,
  updateFavorite: PropTypes.func,
  updateBasket: PropTypes.func,
  openModal: PropTypes.func,
};

export default Product;