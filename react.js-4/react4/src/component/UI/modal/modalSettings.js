const modalSettings = [
  {
    modalId: "default",
    settings: {
      header: "",
      closeButton: true,
      text: (data) => data,
      actions: [
        {
          type: "submit",
          backgroundColor: "white",
          text: "",
        },
        {
          type: "cancel",
          backgroundColor: "white",
          text: "",
        },
      ],
    },
  },
  {
    modalId: "addToBasket",
    settings: {
      header: "Додавання товару до кошика",
      closeButton: true,
      text: (data) => `Ви намагаєтесь додати ${data} до кошика. Продовжити?`,
      actions: [
        {
          type: "submit",
          backgroundColor: "pink",
          text: "Поїхали!",
        },
        {
          type: "cancel",
          backgroundColor: "red",
          text: "Ні, я передумав",
        },
      ],
    },
  },
  {
    modalId: "removeFromBasket",
    settings: {
      header: "Видалення товару з кошика",
      closeButton: true,
      text: (data) => `Ви намагаєтесь видалити ${data} з кошика. Робимо?`,
      actions: [
        {
          type: "submit",
          backgroundColor: "pink",
          text: "Так!",
        },
        {
          type: "cancel",
          backgroundColor: "red",
          text: "Нехай буде",
        },
      ],
    },
  },
];

export default modalSettings;
