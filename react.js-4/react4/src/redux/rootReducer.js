import { combineReducers } from "redux";
import { magazineReducer as magazine } from "./reducers/magazine";

export const rootReducer = combineReducers({magazine});
