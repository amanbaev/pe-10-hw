import React from "react";
import { useSelector } from "react-redux";
import ProductList from "../component/product-list/ProductList";

export function Home() {
  const products = useSelector((state) => state.magazine.products.products);
  return <ProductList page="home" products={products} />;
}
