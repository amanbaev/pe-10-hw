import ProductList from "../component/product-list/ProductList";
import { useSelector } from "react-redux";

export function Favorite() {
  const products = useSelector(
    (state) => state.magazine.products.productsInFavorite
  );

  return products.length ? (
    <ProductList page="favorite" products={products} />
  ) : (
    "Товарів поки немає ):"
  );
}
