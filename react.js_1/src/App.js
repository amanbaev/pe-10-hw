import React, { Component } from 'react';
import Button from './Button';
import Modal from './Modal';


class App extends Component {
  state = {
    firstModalVisible: false,
    secondModalVisible: false,
  };

  FirstModal = () => { this.setState((prevState) => ({ firstModalVisible: !prevState.firstModalVisible, })); };

  SecondModal = () => { this.setState((prevState) => ({ secondModalVisible: !prevState.secondModalVisible, })); };
  
  render () { const { firstModalVisible, secondModalVisible } = this.state; 
  return (
   <div className="app"> 
  <Button backgroundColor="blue" text="Открыть первое модальное окно" onClick={this.FirstModal} /> 
  <Button backgroundColor="green" text="Открыть второе модальное окно" onClick ={this.SecondModal} /> 
  {firstModalVisible && ( <Modal header="Первое модальное окно" closeButton={true} text="Текст для первого модального окна" action={
   <>
     <button onClick={this.FirstModal}> Закрыть </button> 
      <button> Сохранить </button>
   </> 
  } 
  onClose={this.FirstModal} backgroundColor="#f1c40f" /> )} 
  {secondModalVisible && 
  ( <Modal header="Второе модальное окно" closeButton={true} text="Текст для второго модального окна" action= { 
  <>
   <button onClick={this.SecondModal}> Закрыть </button> 
  <button> Удалить </button> 
  </> } onClose={this.SecondModal} backgroundColor="#3498db" /> )}
   </div > 
  ); } }

export default App;
