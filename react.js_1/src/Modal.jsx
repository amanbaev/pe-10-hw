import React, { Component } from "react";
import './Modal.scss';

class Modal extends Component {
  modalRef = React.createRef();

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  handleClickOutside = (event) => {
    if (this.modalRef && !this.modalRef.current.contains(event.target)) {
      this.props.onClose();
    }
  };

  render() {
    const { header, closeButton, text, actions, backgroundColor } = this.props;

    return (
      <div className="modal-overlay">
        <div className="modal" ref={this.modalRef} style={{ backgroundColor }}>
          <div className="modal-header">
            <h2>{header}</h2>
            {closeButton && (
              <button className="modal-close" onClick={this.props.onClose}>X</button>
            )}
          </div>
          <div className="modal-body">{text}</div>
          <div className="modal-foter">{actions}</div>
        </div>
      </div>
    );
  }
}

export default Modal;
