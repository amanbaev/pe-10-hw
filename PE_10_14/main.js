const themeButton = document.getElementById("theme");
const body = document.body;
body.style.backgroundColor = getColor();

if (!localStorage.getItem("background")) {
  localStorage.setItem("background", "white");
}

if ((body.style.backgroundColor = "white")) {
  themeButton.style.backgroundColor = "black";
  themeButton.style.color = "white";
}
if ((body.style.backgroundColor = "black")) {
  themeButton.style.backgroundColor = "white";
  themeButton.style.color = "black";
}

function getColor() {
  return localStorage.getItem("background");
}

themeButton.addEventListener("click", (e) => {
  if (localStorage.getItem("background") === "white") {
    localStorage.setItem("background", "black");
    themeButton.style.backgroundColor = "white";
    themeButton.style.color = "black";
  } else if (localStorage.getItem("background") === "black") {
    localStorage.setItem("background", "white");
    themeButton.style.backgroundColor = "black";
    themeButton.style.color = "white";
  }
  body.style.backgroundColor = getColor();
});