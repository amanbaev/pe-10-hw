class Employee {
    constructor(name, age , salary) {
     this._name = name;
     this._age = age;
     this._salary = salary
    }

    get name() {
        return 'my name is ' + this._name;
    }

    set name(value) {
        this._name = value;
    }

    get age() {
        return 'my age is ' + this._age;
    }

    set age(value) {
        this._age = value;
    }

    get salary() {
        return 'my salary ' + this._salary;
    }

    set salary(value) {
        this._salary = value;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, languages) {
        super(name, age, salary);
        this._languages = languages;
    }
    get languages() {
        return this._languages;
    }
    set languages(value) {
        this._languages =  value;
    }
    get salary() {
        return this._salary*3;
    }
    set salary(value) {
        this._salary = value;
    }
}

let programmer = new Programmer("John Doe", 30, 50000, ["JavaScript", "Python"]);
console.log(programmer.name);
console.log(programmer.age); 
console.log(programmer.salary);
console.log(programmer.languages);

let programmer1 = new Programmer("John Doe", 30, 50000, ["JavaScript", "Python"]);
let programmer2 = new Programmer("Jane Smith", 25, 45000, ["Java", "C++"]);
let programmer3 = new Programmer("Bob Johnson", 35, 55000, ["C#", "Ruby"]);

console.log(programmer1);
console.log(programmer2);
console.log(programmer3);


// №1
// В плане наследования JavaScript работает лишь с одной сущностью: 
// объектами. Каждый объект имеет внутреннюю ссылку на другой объект, 
// называемый его прототипом. У объекта-прототипа также есть свой собственный прототип 
// и так далее до тех пор, пока цепочка не завершится объектом, 
// у которого свойство prototype равно null .

// №2
// Функция super() , возвращает объект-посредник, 
// который делегирует вызовы метода родительскому или родственному классу, 
// указанного type типа.
// Это полезно для доступа к унаследованным методам, 
// которые были переопределены в классе.
